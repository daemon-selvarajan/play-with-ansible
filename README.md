# Play with Ansible

A project which will explain about how to make a basic ansible scripts & execute.

#### Ansible-Introduction

An Ansible playbook is an organized unit of scripts that defines work for a server configuration 
managed by the automation tool Ansible. Ansible is a configuration management tool that automates 
the configuration of multiple servers by the use of Ansible playbooks. ... 
Ansible plays are written in YAML.

Refer below link to know more about ansible 
https://docs.ansible.com/ansible/latest/user_guide/playbooks.html

#### Pre-requisite

- Install python3 or python2   
- Linux Environment

#### Project Steps

##### Step-01 :: Clone play-with-ansible project from gitlab
`git clone git@gitlab.com:daemon-selvarajan/play-with-ansible.git`

##### Step-02 :: Enter into the cloned directory
`cd play-with-ansible`

##### Step-03 :: Install required python packages
`pip install -r requirement.txt`

##### Step-04 :: Edit the host names in `hosts` file
Hosts file will look like below
```
[all:vars]
ansible_ssh_user=<remote_server_username>
ansible_ssh_pass=<remote_server_password>
ansible_become_user=root
ansible_become_pass=<remote_sudo_username>
ansible_become_method=sudo

[AlphaServers]
server-01.selvarajan.net
server-02.selvarajan.net
server-03.selvarajan.net
server-04.selvarajan.net

[productionServers]
server-05.selvarajan.net
server-06.selvarajan.net
server-07.selvarajan.net
server-08.selvarajan.net
```

##### Step-05 :: Run the playbook

- If you want to run the playbook tasks only on AlphaServers group   
   `ansible-playbook -i hosts --limit AlphaServers playbook.yml`
  
- If you want to run the playbook tasks only on productionServers group     
    `ansible-playbook -i hosts --limit productionServers playbook.yml`
  
- For all servers,   
    `ansible-playbook -i hosts playbook.yml`
  
- Redirect to a file for further process, example below   
    `ansible-playbook -i hosts --limit AlphaServers playbook.yml > out.txt`
  
  After redirecting, `out.txt` will looks like below
  ``` 
    PLAY [Some basic works with Ansible Scripts] ******************************************************************************************************************************************************************************************

    TASK [TASK-1 copy sample.txt file to the servers] *************************************************************************************************************************************************************************************
    changed: [ttqc-fus-exe-62]
    changed: [ttqc-fus-exe-61]
    changed: [ttqc-fus-exe-64]
    changed: [ttqc-fus-exe-63]
    
    TASK [TASK-2 Make a run with the copied script] ***************************************************************************************************************************************************************************************
    changed: [ttqc-fus-exe-61]
    changed: [ttqc-fus-exe-62]
    changed: [ttqc-fus-exe-63]
    changed: [ttqc-fus-exe-64]
    
    TASK [TASK-3 Execute some shell command on the the remote host] ***********************************************************************************************************************************************************************
    changed: [ttqc-fus-exe-61]
    changed: [ttqc-fus-exe-62]
    changed: [ttqc-fus-exe-64]
    changed: [ttqc-fus-exe-63]
    
    TASK [TASK-4 Cleanup] *****************************************************************************************************************************************************************************************************************
    changed: [ttqc-fus-exe-61]
    changed: [ttqc-fus-exe-64]
    changed: [ttqc-fus-exe-62]
    changed: [ttqc-fus-exe-63]
    
    PLAY RECAP ****************************************************************************************************************************************************************************************************************************
    ttqc-fus-exe-61            : ok=4    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    ttqc-fus-exe-62            : ok=4    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    ttqc-fus-exe-63            : ok=4    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    ttqc-fus-exe-64            : ok=4    changed=4    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0   
    ```
 
##### Step-06 :: optional step, but recommended

Change the output format in some standard readable format, so that further processing the output will be easier

To do that, add some plugin in the ansible.cfg file 

`sudo vi /etc/ansible/ansible.cfg`

ansible.cfg will looks like below,

```
[defaults]
remote_tmp=/tmp/$USER/.ansible/tmp
host_key_checking = False
log_path=/var/log/ansible/run.log
callback_plugins = /opt/ansible/callback/
forks = 50
timeout= 60
display_skipped_hosts = False
deprecation_warnings=False
ansible_managed = Ansible managed: {file} modified on %Y-%m-%d %H:%M:%S by {uid} on {host}
gathering = smart
retry_files_enabled = False
pipelining=True
internal_poll_interval = 0.001
ansible_python_interpreter=/usr/bin/python3

[ssh_connection]
scp_if_ssh = True
ssh_args = -o ControlMaster=auto -o ControlPersist=60s
pipelining = True
```

Add `stdout_callback=json` under `defaults` tag

Now playbook output will be in json format, we can use it for further process

` ansible-playbook -i hosts --limit AlphaServers playbook.yml > out.json`

Refer out.json for reference